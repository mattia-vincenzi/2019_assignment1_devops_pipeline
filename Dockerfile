FROM openjdk:8
ENV MYSQL_HOST=mysql-server
RUN apt-get update && apt-get install -y curl
COPY ./target/docker-spring-boot.jar ./docker-spring-boot.jar
EXPOSE 8085
ENTRYPOINT ["java","-jar","docker-spring-boot.jar"]