package com.example.accessingdatamysql;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class AccessingDataMysqlTest {

	private User user;

	/**
	 * Constructor.
	 */
	public AccessingDataMysqlTest() {
		user = new User();
	}

	@Test
	public void checkUserID() {
		int id = 9999;
		user.setId(id);
		assertTrue(user.getId().equals(id));
	}

	@Test
	public void checkUserName() {
		String name = "Aldo Rossi";
		user.setName(name);
		assertTrue(user.getName().equals(name));
	}

	@Test
	public void checkUserEmail() {
		String email = "ar97@gmail.com";
		user.setEmail(email);
		assertTrue(user.getEmail().equals(email));
	}
}
