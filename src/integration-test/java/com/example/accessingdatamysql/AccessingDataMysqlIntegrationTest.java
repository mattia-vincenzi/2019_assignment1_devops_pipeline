package com.example.accessingdatamysql;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AccessingDataMysqlIntegrationTest {

  private static final String NAME = "Aldo Rossi";
  private static final String EMAIL = "a.rossi97@example.com";

  @LocalServerPort private int port;
  TestRestTemplate restTemplate = new TestRestTemplate();
  HttpHeaders headers = new HttpHeaders();

  @Test
  public void testCreateUser() throws Exception {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

    MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
    map.add("name", NAME);
    map.add("email", EMAIL);

    HttpEntity<MultiValueMap<String, String>> request =
        new HttpEntity<MultiValueMap<String, String>>(map, headers);

    ResponseEntity<String> response =
        restTemplate.postForEntity(createUrlWithPort("/demo/add"), request, String.class);

    assertTrue(response.getStatusCode().toString().equals("200 OK"));
    assertTrue(response.getBody().equals("Saved"));
  }

  @Test
  public void testRetrieveUser() throws Exception {
    HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    ResponseEntity<String> response =
        restTemplate.exchange(createUrlWithPort("/demo/all"), HttpMethod.GET, entity, String.class);

    // Check Http response code.
    assertTrue(response.getStatusCode().toString().equals("200 OK"));

    // Compare list of objects with expected inserted user.
    String expected = "[{\"id\":1,\"name\":\"Aldo Rossi\",\"email\":\"a.rossi97@example.com\"}]";
    assertEquals(expected, response.getBody());
  }

  // Utility method.
  private String createUrlWithPort(String uri) {
    return "http://localhost:" + port + uri;
  }
}