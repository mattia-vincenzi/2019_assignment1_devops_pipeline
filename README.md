# Simple Spring Webapp
A simple Spring webapp for storing data about its users. Gitlab CI/CD is fully supported!

**Link to GitLab repository:** [click here](https://gitlab.com/mattia-vincenzi/2019_assignment1_devops_pipeline)

**Link to Trello board:** [click here](https://trello.com/b/Jf7pulia/assignment-1-pss)

## Group Components

 - Porto Francesco (816042)
 - Silva Edoardo (816560)
 - Vincenzi Mattia (860579)

## Project description
We have created a simple Spring webapp that stores data (id, name and email) about its users via a MySQL database.

The HTTP GET requests are mapped to **/demo/all**; a JSON file containing the list of all users and their data is returned.

The HTTP POST requests are mapped to **/demo/add**; they are used to add users to the database, by using the simple APIs provided by the Spring Framework.

## Gitlab CI/CD Pipeline

We have implemented the **whole pipeline** as presented in the text of the assignment. Here's a brief description of each step, including some further explaination on some choices we made, and a who-did-what reference for teamwork evaluation:

 - **Build**: Use Maven to build the project and download dependencies.
 - **Verify**: Use a combination of different static and dynamic analysis tools (as Maven plugins) to verify code-style and quality. We use *checkstyle* to make sure the code is well-structured. We also use *PMD* (static analysis) and *Spotbugs* (dynamic analysis) to make sure that the project is as bug-free as possible.
 - **Unit tests**: Check that the User class works properly; we used JUnit.
 - **Integration tests**: Check that the system works correctly as a whole (that is, the program and the MySQL database) by sending HTTP Post requests to add users and HTTP Get requests to check that the users exist; we used JUnit and the MySQL service provided by GitLab.
 - **Package**: Package the webapp in a .jar file. Note that tests are not included in the .jar file because we didn't consider them to be relevant to the release.
 - **Release**: Create a Docker image according to the Dockerfile and push it to the GitLab registry.
 - **Deploy**: Download the Docker image and create a container in the Azure VM. 

> Francesco Porto mostly worked on step 2, 6 and 7.  
> Mattia Vincenzi mostly worked on step 2, 3 and 4.  
> Edoardo Silva mostly worked on step 6 and 7.  

## Some additional notes on Deployment
- **Authentication**: In order to authenticate ourselves, we created a private SSH key (which is stored in a GitLab variable) and sent it to the server. This was repeated 3 times (one for each developer).
- **VM configuration**:  We use a MySQL container called "mysql-server" and a network called "*mysql-network*" to which the other container is connected to. By using the ENV command in the Dockerfile, the webapp knows the name of the MySQL server, which is resolved by Docker's built-in DNS (since we use the *--network* option). 
  >We previously had an issue where we thought the DNS wasn't working, but it was due to a simple mistake in the parameter order (that is -- we were using the --network parameter AFTER the image id, resulting in Docker actually ignoring it).
- **Multi-user Deployment**: The deployment is done on the Azure VM of the developer who pushes the latest commit (by exploiting the GitLab container's underlying Unix bash and using the Gitlab ID to set ssh addresses and ports).


## TODOs for final deadline
For the first deadline we aimed for a basic but fully working pipeline, we'll be making more adjustments in the next few weeks, such as:
 
 - ~~Fixing deployment (maybe it will be already completed in time for the first deadline); note that we have the basic ideas (and Linux commands) down~~ **Done!**
 - ~~Adding more steps to the verify stage~~ **Done!**
 - ~~Making better use of artifacts and caching~~ **Done!**
 - ~~Adding Multiuser Deployment~~ **Done!**